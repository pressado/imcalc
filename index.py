#Calculadora de Índice de Massa Corpórea
print("Bem vindo à Calculadora de IMC\n")

def calculo():
    try:
        peso = float(input('Digite seu peso em Kg: '))
        altura = float(input('Digite sua altura em centímetros: '))
    except:
        print("Digite um valor válido!\n")
        return calculo()

    imc = peso/(altura/100)**2
    imc = round(imc,1)

    if imc < 18.5:
        print("Seu IMC é: ", imc, 'classificado como MAGREZA')
    elif imc >= 18.5 and imc <= 24.9:
        print("Seu IMC é: ", imc, 'classificado como NORMAL')
    elif imc >= 25.0 and imc <= 29.9:
        print("Seu IMC é: ", imc, 'classificado como SOBREPESO')
    elif imc >= 30.0 and imc <= 39.9:
        print("Seu IMC é: ", imc, 'classificado como OBESIDADE')
    elif imc >= 40.0:
        print("Seu IMC é: ", imc, 'classificado como OBESIDADE GRAVE')

    repetir = input('Desejar repetir? [S]im ou [N]ão? \n')
    repetir = repetir.lower()
    if repetir == 's' or repetir == 'sim':
        calculo()
    else:
        exit()

calculo()


